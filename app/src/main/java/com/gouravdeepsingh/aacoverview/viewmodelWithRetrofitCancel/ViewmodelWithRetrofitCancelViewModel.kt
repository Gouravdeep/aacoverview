package com.gouravdeepsingh.aacoverview.viewmodelWithRetrofitCancel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.gouravdeepsingh.aacoverview.utils.Helper
import com.gouravdeepsingh.aacoverview.webservices.ApiClient
import com.gouravdeepsingh.aacoverview.webservices.Resource
import com.gouravdeepsingh.aacoverview.R
import com.gouravdeepsingh.aacoverview.utils.Constants
import com.gouravdeepsingh.aacoverview.webservices.models.ImagesResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.ConnectException
import java.net.SocketTimeoutException

class ViewmodelWithRetrofitCancelViewModel : ViewModel() {
    private  var call:Call<ImagesResponse>? = null
    val imagesLiveData = MutableLiveData<Resource<ImagesResponse>>()

    fun callImagesWebservice(type:String) {
        if (imagesLiveData.value?.data != null && imagesLiveData.value?.data?.status== Constants.SUCCESS){
            return
        }
        imagesLiveData.value = Resource.Loading()
         call = ApiClient.apiInterface?.getImages(type)
         call?.enqueue(object :
            Callback<ImagesResponse> {
            override fun onResponse(call: Call<ImagesResponse>, response: Response<ImagesResponse>) {
                val body = response.body()
                if (body != null && body.status != null) {
                    if (body.status == 200) {
                        imagesLiveData.value = Resource.Success(body)
                    }  else {
                        imagesLiveData.value = Resource.Error(message = body.error?: Helper.getString(
                            R.string.general_error))
                    }
                } else {
                    imagesLiveData.value = Resource.Error(message = Helper.getString(R.string.general_error))
                }
            }

            override fun onFailure(call: Call<ImagesResponse>, t: Throwable) {
                if (!call.isCanceled) {
                    if (t is SocketTimeoutException) {
                        imagesLiveData.value =
                            Resource.Error(message = Helper.getString(R.string.slow_network_error))
                    } else if (t is ConnectException) {
                        imagesLiveData.value =
                            Resource.Error(message = Helper.getString(R.string.no_network_error))
                    } else {
                        imagesLiveData.value =
                            Resource.Error(message = Helper.getString(R.string.general_error))
                    }
                }

            }
        })
    }

    override fun onCleared() {
        call?.cancel()
        super.onCleared()
    }
}
