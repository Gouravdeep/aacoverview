package com.gouravdeepsingh.aacoverview.viewmodelWithCoroutines

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.gouravdeepsingh.aacoverview.utils.Constants
import com.gouravdeepsingh.aacoverview.webservices.Resource
import com.gouravdeepsingh.aacoverview.ImageAdapter

import com.gouravdeepsingh.aacoverview.R
import com.gouravdeepsingh.aacoverview.databinding.ImagesLayoutBinding
import com.gouravdeepsingh.aacoverview.utils.gone
import com.gouravdeepsingh.aacoverview.utils.visible
import com.gouravdeepsingh.aacoverview.webservices.models.ImagesResponse

class ViewmodelWithCoroutinesFragment : Fragment() {

    private var viewBinding: ImagesLayoutBinding? = null
    private var viewModel: ViewmodelWithCoroutinesViewModel? =null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if(viewBinding ==null) {
            viewBinding = DataBindingUtil.inflate(inflater, R.layout.images_layout, container, false)
            viewModel = ViewModelProviders.of(this).get(ViewmodelWithCoroutinesViewModel::class.java)
            viewBinding?.errorLayout?.tryAgain?.setOnClickListener { viewModel?.callImagesWebservice(
                ViewmodelWithCoroutinesFragmentArgs.fromBundle(arguments!!).type) }
            viewBinding?.recyclerView?.layoutManager = LinearLayoutManager(activity)
            viewBinding?.recyclerView?.adapter = ImageAdapter(activity)
            viewModel?.imagesLiveData?.observe(this, Observer { getImagesresponse(it) })
            viewModel?.callImagesWebservice( ViewmodelWithCoroutinesFragmentArgs.fromBundle(arguments!!).type)
        }
        return viewBinding?.root
    }

    private fun getImagesresponse(resource: Resource<ImagesResponse>){
        viewBinding?.progressLayout?.root?.gone()
        viewBinding?.recyclerView?.gone()
        viewBinding?.errorLayout?.root?.gone()
        when(resource.status){
            Constants.LOADING ->{
                viewBinding?.progressLayout?.root?.visible()
            }
            Constants.SUCCESS ->{
                viewBinding?.recyclerView?.visible()
                (viewBinding?.recyclerView?.adapter as? ImageAdapter)?.setData(resource.data?.data)

            }
            Constants.ERROR ->{
                viewBinding?.progressLayout?.root?.gone()
                viewBinding?.errorLayout?.root?.visible()
                viewBinding?.errorLayout?.errorMsg?.setText(resource.message)
            }
        }

    }

}
