package com.gouravdeepsingh.aacoverview.viewmodelWithCoroutines

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.gouravdeepsingh.aacoverview.utils.Helper
import com.gouravdeepsingh.aacoverview.webservices.ApiClient
import com.gouravdeepsingh.aacoverview.webservices.Resource
import com.gouravdeepsingh.aacoverview.R
import com.gouravdeepsingh.aacoverview.utils.Constants
import com.gouravdeepsingh.aacoverview.webservices.models.ImagesResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.lang.Exception
import java.net.ConnectException
import java.net.SocketTimeoutException

class ViewmodelWithCoroutinesViewModel : ViewModel() {
    val imagesLiveData = MutableLiveData<Resource<ImagesResponse>>()

    fun callImagesWebservice(type:String){
        if (imagesLiveData.value?.data != null && imagesLiveData.value?.data?.status==Constants.SUCCESS){
            return
        }
        imagesLiveData.value = Resource.Loading()
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val imagesResponse = ApiClient.apiInterface?.getImagesUsingCoroutines(type)
                successImageResponse(imagesResponse)

            }catch (e:Exception){
               failureImageResponse(e)
            }
        }
    }

     fun successImageResponse(imagesResponse:ImagesResponse?){
        if (imagesResponse != null && imagesResponse.status != null) {
            if (imagesResponse.status == 200) {
                imagesLiveData.postValue(Resource.Success(imagesResponse))
            } else {
                imagesLiveData.postValue(Resource.Error(message = imagesResponse.error ?: Helper.getString(R.string.general_error)))
            }
        } else {
            imagesLiveData.postValue(Resource.Error(message = Helper.getString(R.string.general_error)))
        }
    }

     fun failureImageResponse(e:Exception){
        if (e is SocketTimeoutException){
            imagesLiveData.postValue(Resource.Error(message = Helper.getString(R.string.slow_network_error)))
        }else if(e is ConnectException){
            imagesLiveData.postValue(Resource.Error(message = Helper.getString(R.string.no_network_error)))
        }else{
            imagesLiveData.postValue(Resource.Error(message = Helper.getString(R.string.general_error)))
        }
    }

}
