package com.gouravdeepsingh.aacoverview.webservices

import com.gouravdeepsingh.aacoverview.webservices.models.ImagesResponse
import com.gouravdeepsingh.aacoverview.webservices.models.WallpaperTypeResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query


interface ApiInterface {
    @GET("images")
    fun getImages(@Query("type") type: String): Call<ImagesResponse>

    @GET("images")
    suspend fun getImagesUsingCoroutines(@Query("type") type: String): ImagesResponse?

    @GET("wallpaper-types")
     fun getWallpaperTypes(): Call<WallpaperTypeResponse>

    @GET("wallpaper-types")
    suspend fun getWallpaperTypesUsingCoroutines(): WallpaperTypeResponse?

}