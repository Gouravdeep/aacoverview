package com.gouravdeepsingh.aacoverview.webservices.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

data class WallpaperTypeResponse(
    val `data`: List<WallpaperTypeData?>?,
    val error: String?,
    val status: Int?
) {
    @Entity(tableName = "wallpaper_type_table")
    data class WallpaperTypeData(
        @PrimaryKey
        val id: String,
        val name: String
    )
}