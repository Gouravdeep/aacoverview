package com.gouravdeepsingh.aacoverview.webservices

import com.gouravdeepsingh.aacoverview.utils.Constants

sealed class Resource<T>(
    val status: Int,
    val data: T? = null,
    val message: String?=null
) {
    class Loading<T>() : Resource<T>(Constants.LOADING)
    class Success<T>(data: T, message: String?=null) : Resource<T>(Constants.SUCCESS, data, message)
    class Error<T>(data: T? = null, message: String?=null) : Resource<T>(Constants.ERROR, data, message)
    class Transfer<T>(status: Int, data: T?=null,message: String?=null) : Resource<T>(status, data, message)
}