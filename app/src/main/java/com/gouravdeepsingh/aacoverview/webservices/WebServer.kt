package com.gouravdeepsingh.aacoverview.webservices

import com.gouravdeepsingh.aacoverview.utils.MyApplication
import okhttp3.mockwebserver.Dispatcher
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okhttp3.mockwebserver.RecordedRequest
import java.util.concurrent.TimeUnit


object WebServer {

    var DELAY_TIME = 2L

    private var application: MyApplication? = null
    private val server = MockWebServer()

     fun startServer(application: MyApplication){
         this.application = application

        val dispatcher = object : Dispatcher() {
            override fun dispatch(request: RecordedRequest) =
                if (request.path?.contains("images")?:false){
                    MockResponse().setResponseCode(200).setBody(
                        readFile("images.json")).setBodyDelay(DELAY_TIME,TimeUnit.SECONDS)
                }
                else{
                    MockResponse().setResponseCode(200).setBody(
                        readFile("wallpaper_types.json")).setBodyDelay(DELAY_TIME,TimeUnit.SECONDS)
                }

            }

        server.dispatcher = dispatcher
         Thread(Runnable {
             server.start(8080)
         }).start()
    }

    fun stopServer(){
        server.shutdown()
    }

    private fun readFile(fileName:String):String{
        return application?.assets?.open(fileName)?.bufferedReader().use{
            it?.readText()
        }?:""
    }

}