package com.gouravdeepsingh.aacoverview.webservices.models


data class ImagesResponse(
    val `data`: List<Data?>?,
    val error: String?,
    val status: Int?
) {
    data class Data(
        val fav: Int?,
        val is_fav: Int?,
        val rating: Float?,
        val sees: Int?,
        val url: String?
    )
}