package com.gouravdeepsingh.aacoverview

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.gouravdeepsingh.aacoverview.databinding.RecyclerViewItemBinding
import com.gouravdeepsingh.aacoverview.webservices.models.ImagesResponse

class ImageAdapter( val context: Context?) : RecyclerView.Adapter<ImageAdapter.ViewHolder>() {

    var imageList: List<ImagesResponse.Data?>? = null

    class ViewHolder (val itemBinding: RecyclerViewItemBinding) : RecyclerView.ViewHolder(itemBinding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(DataBindingUtil.inflate(LayoutInflater.from(context),R.layout.recycler_view_item, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = imageList?.get(position)
        holder.itemBinding.data = item
        holder.itemBinding.executePendingBindings()

    }

    fun setData(imageList: List<ImagesResponse.Data?>?){
        this.imageList = imageList
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return imageList?.size?:0
    }
}