package com.gouravdeepsingh.aacoverview.utils

import android.app.Application
import android.content.Context
import android.util.Log
import androidx.work.*
import com.gouravdeepsingh.aacoverview.database.AppDatabase
import com.gouravdeepsingh.aacoverview.webservices.ApiClient
import kotlinx.coroutines.coroutineScope
import java.util.*
import java.util.concurrent.TimeUnit

class WallpaperTypeCoroutinesWorker (context: Context, params: WorkerParameters) : CoroutineWorker(context, params) {

    companion object{
        fun startWorker(application: Application){
            if (SharedPreferenceUtils.getData(SharedPreferenceUtils.PREODIC_WORKER_ID,"").isEmpty()) {
                val constraints = Constraints.Builder()
                    .setRequiredNetworkType(NetworkType.CONNECTED)
                    .build()

                val saveRequest = PeriodicWorkRequest.Builder(
                    WallpaperTypeCoroutinesWorker::class.java,
                    12,
                    TimeUnit.HOURS
                )
                    .setConstraints(constraints)
                    .build()

                WorkManager.getInstance(application).enqueue(saveRequest)
            }
        }

        fun stopWorker(application: Application){
           val id =  SharedPreferenceUtils.getData(SharedPreferenceUtils.PREODIC_WORKER_ID,"")
            if (id.isNotEmpty()) {
                WorkManager.getInstance(application).cancelWorkById(UUID.fromString(id))
            }
        }
    }

    override suspend fun doWork(): Result = coroutineScope {
        Log.e("tagg periodic", id.toString())
        try {
            val data = ApiClient.apiInterface?.getWallpaperTypesUsingCoroutines()
            if (data?.status ?: 0 == 200) {
                AppDatabase.db?.wallpaperTypeDao()?.saveWallpaperTypeList(data?.data)
                Result.success()
            } else {
                Result.retry()
            }
        }catch (e:Exception){
            Result.retry()
        }

    }
}