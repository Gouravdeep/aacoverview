package com.gouravdeepsingh.aacoverview.utils

import android.app.Application
import com.gouravdeepsingh.aacoverview.webservices.WebServer
import com.gouravdeepsingh.aacoverview.database.AppDatabase

class MyApplication: Application() {
    override fun onCreate() {
        super.onCreate()
        WebServer.startServer(this)
        Helper.init(this)
        AppDatabase.init(this)
    }
}