package com.gouravdeepsingh.aacoverview.utils

import android.app.Application
import android.content.Context
import android.util.Log
import androidx.work.*
import com.gouravdeepsingh.aacoverview.database.AppDatabase
import com.gouravdeepsingh.aacoverview.webservices.ApiClient
import java.util.concurrent.TimeUnit

class WallpaperTypeWorker (appContext: Context, workerParams: WorkerParameters) : Worker(appContext, workerParams) {

    companion object{
        fun startWorker(application: Application){
            val constraints = Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build()

            val saveRequest = OneTimeWorkRequest.Builder(WallpaperTypeWorker::class.java)
                    .setConstraints(constraints)
                    .build()

            WorkManager.getInstance(application).
                beginUniqueWork("save-wallpaper-type-worker", ExistingWorkPolicy.REPLACE,saveRequest)
                .enqueue()
        }
    }

    override fun doWork(): Result {
        try {
            Log.e("tagg onetimer", id.toString())
            val data = ApiClient.apiInterface?.getWallpaperTypes()?.execute()?.body()
            if (data?.status ?: 0 == 200) {
                AppDatabase.db?.wallpaperTypeDao()?.saveWallpaperTypeList(data?.data)
                return Result.success()
            } else {
                return Result.retry()
            }
        }catch (e:Exception){
            return Result.retry()
        }
    }
}