package com.gouravdeepsingh.aacoverview.utils

import android.widget.ImageView
import com.bumptech.glide.Glide

object Helper {

    private var application: MyApplication? = null

    fun init(application: MyApplication){
        this.application = application
    }

    fun getString(id:Int) = application?.resources?.getString(id)?:""

    fun displayImage(imageView:ImageView?,imageUrl: String?) {
        if (application != null && imageView != null && imageUrl != null) {
            Glide.with(application!!).load(imageUrl).into(imageView)
        }
    }
}