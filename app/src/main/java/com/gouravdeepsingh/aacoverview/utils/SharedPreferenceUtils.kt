package com.gouravdeepsingh.aacoverview.utils

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit

object SharedPreferenceUtils {

    val PREODIC_WORKER_ID = "PREODIC_WORKER_ID"

    private var sharedPreferences:SharedPreferences?=null
    fun init(application: MyApplication){
        sharedPreferences= application.getSharedPreferences("pref", Context.MODE_PRIVATE)
    }

    fun setData(key:String,value:String)=sharedPreferences?.edit { putString(key, value) }

    fun getData(key:String,defValue:String="")=sharedPreferences?.getString(key, defValue)?:defValue
}