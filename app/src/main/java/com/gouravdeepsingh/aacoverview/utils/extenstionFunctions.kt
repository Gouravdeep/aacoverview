package com.gouravdeepsingh.aacoverview.utils

import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter


fun View.visible(){
    if(this.visibility != View.VISIBLE) {
        this.visibility = View.VISIBLE
    }
}

fun View.gone(){
    if(this.visibility != View.GONE) {
        this.visibility = View.GONE
    }
}

@BindingAdapter("imageUrl")
fun ImageView.loadImage( imageUrl: String?) {
    Helper.displayImage(this,imageUrl)
}