package com.gouravdeepsingh.aacoverview

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.gouravdeepsingh.aacoverview.utils.Helper
import com.gouravdeepsingh.aacoverview.webservices.ApiClient
import com.gouravdeepsingh.aacoverview.databinding.ImagesLayoutBinding
import com.gouravdeepsingh.aacoverview.databinding.RecyclerViewItemBinding
import com.gouravdeepsingh.aacoverview.utils.gone
import com.gouravdeepsingh.aacoverview.utils.visible
import com.gouravdeepsingh.aacoverview.webservices.models.ImagesResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.ConnectException
import java.net.SocketTimeoutException

class MVCFragment : Fragment() {

    private var viewBinding:ImagesLayoutBinding? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if(viewBinding ==null) {
            viewBinding = DataBindingUtil.inflate(inflater, R.layout.images_layout, container, false)
            viewBinding?.errorLayout?.tryAgain?.setOnClickListener { callMovieWebservice() }
            viewBinding?.recyclerView?.layoutManager = LinearLayoutManager(activity)
            viewBinding?.recyclerView?.adapter = ImageAdapter(activity)
            callMovieWebservice()
        }
        return viewBinding?.root
    }

    private fun callMovieWebservice() {
        viewBinding?.progressLayout?.root?.visible()
        viewBinding?.recyclerView?.gone()
        viewBinding?.errorLayout?.root?.gone()
        ApiClient.apiInterface?.getImages(MVCFragmentArgs.fromBundle(arguments!!).type)?.enqueue(object : Callback<ImagesResponse> {
            override fun onResponse(call: Call<ImagesResponse>, response: Response<ImagesResponse>) {
                val body = response.body()
                if (body != null && body.status != null) {
                    if (body.status == 200) {
                        viewBinding?.progressLayout?.root?.gone()
                        viewBinding?.recyclerView?.visible()
                        (viewBinding?.recyclerView?.adapter as? ImageAdapter)?.setData(body.data)
                    }  else {
                        errorResponse(body.error)
                    }
                } else {
                    errorResponse(Helper.getString(R.string.general_error))
                }
            }

            override fun onFailure(call: Call<ImagesResponse>, t: Throwable) {
                if (t is SocketTimeoutException){
                    errorResponse(Helper.getString(R.string.slow_network_error))
                }else if(t is ConnectException){
                    errorResponse(Helper.getString(R.string.no_network_error))
                }else{
                    errorResponse(Helper.getString(R.string.general_error))
                }

            }
        })
    }

    private fun errorResponse(msg:String?){
        viewBinding?.progressLayout?.root?.gone()
        viewBinding?.errorLayout?.root?.visible()
        viewBinding?.errorLayout?.errorMsg?.setText(msg)
    }



}
