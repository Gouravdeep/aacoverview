package com.gouravdeepsingh.aacoverview.viewmodelWithInterface

import androidx.lifecycle.ViewModel
import com.gouravdeepsingh.aacoverview.utils.Helper
import com.gouravdeepsingh.aacoverview.webservices.ApiClient
import com.gouravdeepsingh.aacoverview.R
import com.gouravdeepsingh.aacoverview.webservices.models.ImagesResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.ConnectException
import java.net.SocketTimeoutException

class ViewModelWithInterfecViewModel : ViewModel() {

    fun callMovieWebservice(type:String,webserviceResponse: WebserviceResponse<ImagesResponse>) {
        webserviceResponse.loading()
        ApiClient.apiInterface?.getImages(type)?.enqueue(object :
            Callback<ImagesResponse> {
            override fun onResponse(call: Call<ImagesResponse>, response: Response<ImagesResponse>) {
                val body = response.body()
                if (body != null && body.status != null) {
                    if (body.status == 200) {
                        webserviceResponse.success(body)
                    }  else {
                        webserviceResponse.failure(body.error?:Helper.getString(R.string.general_error))
                    }
                } else {
                    webserviceResponse.failure(Helper.getString(R.string.general_error))
                }
            }

            override fun onFailure(call: Call<ImagesResponse>, t: Throwable) {
                if (t is SocketTimeoutException){
                    webserviceResponse.failure(Helper.getString(R.string.slow_network_error))
                }else if(t is ConnectException){
                    webserviceResponse.failure(Helper.getString(R.string.no_network_error))
                }else{
                    webserviceResponse.failure(Helper.getString(R.string.general_error))
                }

            }
        })
    }
}
