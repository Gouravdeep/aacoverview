package com.gouravdeepsingh.aacoverview.viewmodelWithInterface

interface WebserviceResponse<T> {
    fun loading()
    fun success(data:T)
    fun failure(msg:String)
}