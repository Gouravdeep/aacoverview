package com.gouravdeepsingh.aacoverview.viewmodelWithInterface

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.gouravdeepsingh.aacoverview.ImageAdapter

import com.gouravdeepsingh.aacoverview.R
import com.gouravdeepsingh.aacoverview.databinding.ImagesLayoutBinding
import com.gouravdeepsingh.aacoverview.utils.gone
import com.gouravdeepsingh.aacoverview.utils.visible
import com.gouravdeepsingh.aacoverview.webservices.models.ImagesResponse

class ViewModelWithInterfecFragment : Fragment() {

    private var viewBinding: ImagesLayoutBinding? = null
    private  var viewModel: ViewModelWithInterfecViewModel? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if(viewBinding ==null) {
            viewBinding = DataBindingUtil.inflate(inflater, R.layout.images_layout, container, false)
            viewModel = ViewModelProviders.of(this).get(ViewModelWithInterfecViewModel::class.java)
            viewBinding?.errorLayout?.tryAgain?.setOnClickListener { viewModel?.callMovieWebservice(
                ViewModelWithInterfecFragmentArgs.fromBundle(arguments!!).type,webResponse) }
            viewBinding?.recyclerView?.layoutManager = LinearLayoutManager(activity)
            viewBinding?.recyclerView?.adapter = ImageAdapter(activity)
            viewModel?.callMovieWebservice( ViewModelWithInterfecFragmentArgs.fromBundle(arguments!!).type,webResponse)
        }
        return viewBinding?.root
    }


    val webResponse = object : WebserviceResponse<ImagesResponse>{
        override fun loading() {
            viewBinding?.progressLayout?.root?.visible()
            viewBinding?.recyclerView?.gone()
            viewBinding?.errorLayout?.root?.gone()
        }

        override fun  success(data: ImagesResponse) {
            viewBinding?.progressLayout?.root?.gone()
            viewBinding?.recyclerView?.visible()
            (viewBinding?.recyclerView?.adapter as? ImageAdapter)?.setData(data.data)
        }

        override fun failure(msg: String) {
            viewBinding?.progressLayout?.root?.gone()
            viewBinding?.errorLayout?.root?.visible()
            viewBinding?.errorLayout?.errorMsg?.setText(msg)
        }
    }

}
