package com.gouravdeepsingh.aacoverview

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import com.gouravdeepsingh.aacoverview.database.AppDatabase
import com.gouravdeepsingh.aacoverview.webservices.WebServer
import com.gouravdeepsingh.aacoverview.databinding.FragmentMainBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch

class MainFragment : Fragment() {

    private var viewBinding:FragmentMainBinding? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if(viewBinding ==null) {
            viewBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_main, container, false)
            viewBinding?.webTime?.setText(WebServer.DELAY_TIME.toString())
            viewBinding?.webPlus?.setOnClickListener {
                WebServer.DELAY_TIME = WebServer.DELAY_TIME+1
                viewBinding?.webTime?.setText(WebServer.DELAY_TIME.toString())
            }
            viewBinding?.webMinus?.setOnClickListener {
                WebServer.DELAY_TIME = WebServer.DELAY_TIME-1
                viewBinding?.webTime?.setText(WebServer.DELAY_TIME.toString())
            }

            viewBinding?.mvcPattern?.setOnClickListener {
                val navDirections = MainFragmentDirections.actionMainFragmentToMVCFragment("1")
                viewBinding?.root?.findNavController()?.navigate(navDirections)
            }

            viewBinding?.viewmodelWithInterface?.setOnClickListener {
                val navDirections = MainFragmentDirections.actionMainFragmentToViewModelWithInterfecFragment("1")
                viewBinding?.root?.findNavController()?.navigate(navDirections)
            }

            viewBinding?.viewmodelWithLivedata?.setOnClickListener {
                val navDirections = MainFragmentDirections.actionMainFragmentToViewmodelWithLivedataFragment("1")
                viewBinding?.root?.findNavController()?.navigate(navDirections)
            }

            viewBinding?.viewmodelWithRetrofitCancel?.setOnClickListener {
                val navDirections = MainFragmentDirections.actionMainFragmentToViewmodelWithRetrofitCancelFragment("1")
                viewBinding?.root?.findNavController()?.navigate(navDirections)
            }

            viewBinding?.viewmodelWithCoroutines?.setOnClickListener {
                val navDirections = MainFragmentDirections.actionMainFragmentToViewmodelWithCoroutinesFragment("1")
                viewBinding?.root?.findNavController()?.navigate(navDirections)
            }

            viewBinding?.viewmodelWithRepository?.setOnClickListener {
                val navDirections = MainFragmentDirections.actionMainFragmentToViewmodelWithRepositoryFragment("1")
                viewBinding?.root?.findNavController()?.navigate(navDirections)
            }

            viewBinding?.database?.setOnClickListener {
                val navDirections = MainFragmentDirections.actionMainFragmentToWallpaperTypeFragment()
                viewBinding?.root?.findNavController()?.navigate(navDirections)
            }

            viewBinding?.deleteDatabase?.setOnClickListener {
                viewLifecycleOwner.lifecycleScope.launch(Dispatchers.IO) {
                    AppDatabase.db?.wallpaperTypeDao()?.deleteAllWallpapertypeFromDB()
                }
            }

        }
        return viewBinding?.root
    }

}
