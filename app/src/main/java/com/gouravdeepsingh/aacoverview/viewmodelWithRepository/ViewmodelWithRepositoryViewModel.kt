package com.gouravdeepsingh.aacoverview.viewmodelWithRepository

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.gouravdeepsingh.aacoverview.utils.Helper
import com.gouravdeepsingh.aacoverview.webservices.Resource
import com.gouravdeepsingh.aacoverview.R
import com.gouravdeepsingh.aacoverview.utils.Constants
import com.gouravdeepsingh.aacoverview.webservices.models.ImagesResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import java.net.ConnectException
import java.net.SocketTimeoutException

class ViewmodelWithRepositoryViewModel : ViewModel() {
    val imagesLiveData = MutableLiveData<Resource<ImagesResponse>>()

    @ExperimentalCoroutinesApi
    fun callImagesWebservice(type:String){
        if (imagesLiveData.value?.data != null && imagesLiveData.value?.data?.status== Constants.SUCCESS){
            return
        }
        imagesLiveData.value = Resource.Loading()
        viewModelScope.launch(Dispatchers.IO) {
            WallpaperRepository.getImagesUsingCoroutines(type)
                .onEach { successImageResponse(it) }
                .catch { failureImageResponse(it)}
                .collect()
        }
    }

    fun successImageResponse(imagesResponse: ImagesResponse?){
        if (imagesResponse != null && imagesResponse.status != null) {
            if (imagesResponse.status == 200) {
                imagesLiveData.postValue(Resource.Success(imagesResponse))
            } else {
                imagesLiveData.postValue(Resource.Error(message = imagesResponse.error ?: Helper.getString(
                    R.string.general_error)))
            }
        } else {
            imagesLiveData.postValue(Resource.Error(message = Helper.getString(R.string.general_error)))
        }
    }

    fun failureImageResponse(e: Throwable){
        if (e is SocketTimeoutException){
            imagesLiveData.postValue(Resource.Error(message = Helper.getString(R.string.slow_network_error)))
        }else if(e is ConnectException){
            imagesLiveData.postValue(Resource.Error(message = Helper.getString(R.string.no_network_error)))
        }else{
            imagesLiveData.postValue(Resource.Error(message = Helper.getString(R.string.general_error)))
        }
    }
}
