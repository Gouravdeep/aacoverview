package com.gouravdeepsingh.aacoverview.viewmodelWithRepository

import com.gouravdeepsingh.aacoverview.webservices.ApiClient
import com.gouravdeepsingh.aacoverview.webservices.models.ImagesResponse
import com.gouravdeepsingh.aacoverview.webservices.models.WallpaperTypeResponse
import com.gouravdeepsingh.aacoverview.database.AppDatabase
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow

object WallpaperRepository {

    fun getImagesUsingCoroutines(type: String): Flow<ImagesResponse?> = flow { // flow builder
        val data = ApiClient.apiInterface?.getImagesUsingCoroutines(type)
        emit(data)
    }

    fun getWallpaperTypesUsingCoroutines(): Flow<WallpaperTypeResponse?> = flow { // flow builder
        AppDatabase.db?.wallpaperTypeDao()?.getWallpaperTypeListWithFlow()?.collect{
            if (it?.size?:0>0){
                val wallpaperTypeResponse = WallpaperTypeResponse(it,null,200)
                emit(wallpaperTypeResponse)
                return@collect
            }else{
                val data = ApiClient.apiInterface?.getWallpaperTypesUsingCoroutines()
                if (data?.data?.size?:0>0) {
                    AppDatabase.db?.wallpaperTypeDao()?.saveWallpaperTypeList(data?.data)
                }else{
                    emit(data)
                }
            }
        }
    }

}