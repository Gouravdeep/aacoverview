package com.gouravdeepsingh.aacoverview.viewmodelWithRepository

import android.content.Context
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.gouravdeepsingh.aacoverview.utils.Constants
import com.gouravdeepsingh.aacoverview.webservices.Resource

import com.gouravdeepsingh.aacoverview.R
import com.gouravdeepsingh.aacoverview.databinding.ImagesLayoutBinding
import com.gouravdeepsingh.aacoverview.databinding.WallpaperTypeRecyclerItemBinding
import com.gouravdeepsingh.aacoverview.utils.gone
import com.gouravdeepsingh.aacoverview.utils.visible
import com.gouravdeepsingh.aacoverview.webservices.models.WallpaperTypeResponse

class WallpaperTypeFragment : Fragment() {

    private var viewBinding: ImagesLayoutBinding? = null
    private var viewModel: WallpaperTypeViewModel? =null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if(viewBinding ==null) {
            viewBinding = DataBindingUtil.inflate(inflater, R.layout.images_layout, container, false)
            viewModel = ViewModelProviders.of(this).get(WallpaperTypeViewModel::class.java)
            viewBinding?.errorLayout?.tryAgain?.setOnClickListener { viewModel?.callImagesWebservice() }
            viewBinding?.recyclerView?.layoutManager = LinearLayoutManager(activity)
            viewBinding?.recyclerView?.adapter = WallpaperTypeAdapter()
            viewModel?.imagesLiveData?.observe(this, Observer { getWallpaperTyperesponse(it) })
            viewModel?.callImagesWebservice()
        }
        return viewBinding?.root
    }

    private fun getWallpaperTyperesponse(resource: Resource<WallpaperTypeResponse>){
        viewBinding?.progressLayout?.root?.gone()
        viewBinding?.recyclerView?.gone()
        viewBinding?.errorLayout?.root?.gone()
        when(resource.status){
            Constants.LOADING ->{
                viewBinding?.progressLayout?.root?.visible()
            }
            Constants.SUCCESS ->{
                viewBinding?.recyclerView?.visible()
                (viewBinding?.recyclerView?.adapter as? WallpaperTypeAdapter)?.setData(resource.data?.data)
            }
            Constants.ERROR ->{
                viewBinding?.progressLayout?.root?.gone()
                viewBinding?.errorLayout?.root?.visible()
                viewBinding?.errorLayout?.errorMsg?.setText(resource.message)
            }
        }

    }


    inner private class WallpaperTypeAdapter : RecyclerView.Adapter<WallpaperTypeAdapter.ViewHolder>() {

        var typeList: List<WallpaperTypeResponse.WallpaperTypeData?>? = null

        inner private class ViewHolder (val itemBinding: WallpaperTypeRecyclerItemBinding) : RecyclerView.ViewHolder(itemBinding.root)

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            return ViewHolder(DataBindingUtil.inflate(LayoutInflater.from(activity),R.layout.wallpaper_type_recycler_item, parent, false))
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val item = typeList?.get(position)
            holder.itemBinding.name = item?.name
            holder.itemView.setOnClickListener {
                if(item?.id != null) {
                    val navDirections = WallpaperTypeFragmentDirections.actionWallpaperTypeFragmentToViewmodelWithSaveStateModuleFragment(item.id)
                    viewBinding?.root?.findNavController()?.navigate(navDirections)
                }
            }
            holder.itemBinding.executePendingBindings()

        }

        fun setData(typeList: List<WallpaperTypeResponse.WallpaperTypeData?>?){
            this.typeList = typeList
            notifyDataSetChanged()
        }

        override fun getItemCount(): Int {
            return typeList?.size?:0
        }
    }

}
