package com.gouravdeepsingh.aacoverview.viewmodelWithSaveStateModule

import android.content.Intent
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.SavedStateViewModelFactory
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.gouravdeepsingh.aacoverview.ImageAdapter

import com.gouravdeepsingh.aacoverview.R
import com.gouravdeepsingh.aacoverview.databinding.ImagesLayoutBinding
import com.gouravdeepsingh.aacoverview.utils.Constants
import com.gouravdeepsingh.aacoverview.utils.gone
import com.gouravdeepsingh.aacoverview.utils.visible
import com.gouravdeepsingh.aacoverview.viewmodelWithRepository.ViewmodelWithRepositoryFragmentArgs
import com.gouravdeepsingh.aacoverview.viewmodelWithRepository.ViewmodelWithRepositoryViewModel
import com.gouravdeepsingh.aacoverview.webservices.Resource
import com.gouravdeepsingh.aacoverview.webservices.models.ImagesResponse

class ViewmodelWithSaveStateModuleFragment : Fragment() {

    private var viewBinding: ImagesLayoutBinding? = null
    private var viewModel: ViewmodelWithSaveStateModuleViewModel? =null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if(viewBinding ==null) {
            viewBinding = DataBindingUtil.inflate(inflater, R.layout.images_layout, container, false)
            viewModel = ViewModelProvider(this,SavedStateViewModelFactory(activity?.application!!, this,arguments))
                .get(ViewmodelWithSaveStateModuleViewModel::class.java)
            viewBinding?.errorLayout?.tryAgain?.setOnClickListener { viewModel?.callImagesWebservice()}
            viewBinding?.recyclerView?.layoutManager = LinearLayoutManager(activity)
            viewBinding?.recyclerView?.adapter = ImageAdapter(activity)
            viewModel?.imagesLiveData?.observe(this, Observer { getImagesresponse(it) })
            viewModel?.callImagesWebservice()
        }
        return viewBinding?.root
    }

    private fun getImagesresponse(resource: Resource<ImagesResponse>){
        viewBinding?.progressLayout?.root?.gone()
        viewBinding?.recyclerView?.gone()
        viewBinding?.errorLayout?.root?.gone()
        when(resource.status){
            Constants.LOADING ->{
                viewBinding?.progressLayout?.root?.visible()
            }
            Constants.SUCCESS ->{
                viewBinding?.recyclerView?.visible()
                (viewBinding?.recyclerView?.adapter as? ImageAdapter)?.setData(resource.data?.data)

            }
            Constants.ERROR ->{
                viewBinding?.progressLayout?.root?.gone()
                viewBinding?.errorLayout?.root?.visible()
                viewBinding?.errorLayout?.errorMsg?.setText(resource.message)
            }
        }

    }

}
