package com.gouravdeepsingh.aacoverview.database.daos

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.gouravdeepsingh.aacoverview.webservices.models.WallpaperTypeResponse
import kotlinx.coroutines.flow.Flow

@Dao
interface WallpaperTypeDao {

    @Insert(onConflict = REPLACE)
    fun saveWallpaperTypeList(movieList: List<WallpaperTypeResponse.WallpaperTypeData?>?)

    @Query("SELECT * FROM wallpaper_type_table")
    fun getWallpaperTypeList(): List<WallpaperTypeResponse.WallpaperTypeData?>?

    @Query("SELECT * FROM wallpaper_type_table")
    fun getWallpaperTypeListWithLivedata(): LiveData<List<WallpaperTypeResponse.WallpaperTypeData?>?>?

    @Query("SELECT * FROM wallpaper_type_table")
     fun getWallpaperTypeListWithFlow(): Flow<List<WallpaperTypeResponse.WallpaperTypeData?>?>?

    @Query("DELETE FROM wallpaper_type_table")
    fun deleteAllWallpapertypeFromDB()
}