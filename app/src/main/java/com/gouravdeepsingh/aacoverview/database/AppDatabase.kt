package com.gouravdeepsingh.aacoverview.database

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.gouravdeepsingh.aacoverview.utils.MyApplication
import com.gouravdeepsingh.aacoverview.webservices.models.WallpaperTypeResponse
import com.gouravdeepsingh.aacoverview.database.daos.WallpaperTypeDao

@Database(entities = arrayOf(WallpaperTypeResponse.WallpaperTypeData::class), version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun wallpaperTypeDao(): WallpaperTypeDao

    companion object {
        private var application: MyApplication? = null
        fun init(application: MyApplication) {
            this.application = application
        }

        val db: AppDatabase? by lazy {
            Room.databaseBuilder(application!!, AppDatabase::class.java, "accoverview-database").build()
        }
    }

}