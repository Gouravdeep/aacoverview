package com.gouravdeepsingh.aacoverview

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.gouravdeepsingh.aacoverview.utils.WallpaperTypeCoroutinesWorker
import com.gouravdeepsingh.aacoverview.utils.WallpaperTypeWorker

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        WallpaperTypeCoroutinesWorker.startWorker(this.application)
        WallpaperTypeWorker.startWorker(this.application)
    }

    override fun onDestroy() {
        WallpaperTypeCoroutinesWorker.stopWorker(this.application)
        super.onDestroy()
    }

}
